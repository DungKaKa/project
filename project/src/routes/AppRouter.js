import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import BookTicket from '../views/BookTicket/BookTicket';
import AboutUs from '../views/AboutUs/AboutUs';
import Home from '../views/Home/Home';

class AppRouter extends Component {
    render() {
        return (
                <div>
                    <Route path="/home" exact component={Home} />
                    <Route path="/bookticket/" component={BookTicket} />
                    <Route path="/about/" component={AboutUs} />
                </div>
        );
    }
}

export default AppRouter;