import React, { Component } from 'react';
import '../.././assets/css/login.css';
import '../.././assets/js/login.js';

class ModalLogin extends Component {

    
    render() {
        return (
            <div className="modal fade" id="modelId" tabIndex={-1} role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-md-6 left-side">
                                    <div className="content">
                                        <h5>Having a account help you have the better experience</h5>
                                        <div className="outs_more-buttn"><a href="about.html">SIGNUP</a></div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="wrap-login100">
                                        <form className="login100-form validate-form">
                                            {/* Title Login */}
                                            <span className="login100-form-title">
                                                Login
                                            </span>
                                            {/* Form Login */}
                                            <div className="wrap-input100 validate-input m-b-23" data-validate="Username is reauired">
                                                <span className="label-input100">Username</span>
                                                <input className="input100" type="text" name="username" placeholder="Type your username" required />
                                                <span className="focus-input100" data-symbol="" />
                                            </div>
                                            <div className="wrap-input100 validate-input" data-validate="Password is required">
                                                <span className="label-input100">Password</span>
                                                <input className="input100" type="password" name="pass" placeholder="Type your password" required />
                                                <span className="focus-input100" data-symbol="" />
                                            </div>
                                            {/* Forgot Password */}
                                            <div className="text-right">
                                                <a href="#">
                                                    Forgot password?
                  </a>
                                            </div>
                                            {/* Button Login */}
                                            <div className="container-login100-form-btn">
                                                <div className="wrap-login100-form-btn">
                                                    <div className="login100-form-bgbtn" />
                                                    <button className="login100-form-btn">
                                                        Login
                    </button>
                                                </div>
                                            </div>
                                            {/* Login by FABEOOK */}
                                            <div className="txt1 text-center">
                                                <span>
                                                    Or Sign Up Using
                  </span>
                                            </div>
                                            <div className="flex-c-m">
                                                <a href="#" className="login100-social-item bg1">
                                                    <i className="fab fa-facebook-f" />
                                                </a>
                                                <a href="#" className="login100-social-item bg3">
                                                    <i className="fab fa-google" />
                                                </a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default ModalLogin;