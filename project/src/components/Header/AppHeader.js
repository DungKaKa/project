import React, { Component } from 'react';
import Header from './Header';
import Navigation from './Navigation';

class AppHeader extends Component {
    render() {
        return (
            <div className="header-bar">
                <Header></Header>
                <Navigation></Navigation>
            </div>
        );
    }
}

export default AppHeader;