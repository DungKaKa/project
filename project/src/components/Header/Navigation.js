import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Navigation extends Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>
                <div className="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
                    <ul className="navbar-nav">
                    
                        <NavLink className="nav-item" to="/home">
                            <span className="nav-link">Home</span>
                        </NavLink>

                        <NavLink className="nav-item" to="/bookticket">
                            <span className="nav-link">Book Ticket</span>
                        </NavLink>

                        <NavLink className="nav-item" to="/services">
                            <span className="nav-link">Service/Routes</span>
                        </NavLink>

                        <NavLink className="nav-item" to="/about">
                            <span className="nav-link">About Us</span>
                        </NavLink>

                        <NavLink className="nav-item" to="/something">
                            <span className="nav-link">Something</span>
                        </NavLink>

                    </ul>
                </div>
            </nav>

        );
    }
}

export default Navigation;