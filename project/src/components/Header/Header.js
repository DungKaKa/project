import React, { Component } from 'react';

class Header extends Component {
    render() {
        return (
            <div className="container">
                <div className="hedder-up row">
                    {/*Logo*/}
                    <div className="col-4 logo-head">
                        <h1><a className="navbar-brand" href="/"><span><img src="images/minivan.png" width="20%" /></span>ICT TRANSPORT</a></h1>
                    </div>
                    {/*Login*/}
                    <div className="col-8 right-side-cart">
                        <div className="cart-icons">
                            <ul>
                                <li className="login">
                                    <a href="/" data-toggle="modal" data-target="#modelId"><span><i className="fas fa-user" /></span>
                                        LOGIN</a>
                                </li>
                                <li className="signup">
                                    <a href="/"><span><i className="fas fa-sign-in-alt" /></span>
                                        SIGNUP</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Header;