import React, { Component } from 'react';
import '../.././assets/css/footer.css';

export default class Footer extends Component {
    render() {
        return (
            <div>
                <footer style={{background: 'url(.././images/anh_footer.jpg)'}}>
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-4 col-xs-12">
                                <div className="footerContent left">
                                    <h5 />
                                    <a href="/" className="footer-logo"><img src="images/minivan.png" alt="logo-hl" width="70%" style={{margin: '25px 0px 10px 0px'}}/></a>
                                    <p className="title">ICT Transport Company <br /> Fax: 88888888 </p>
                                </div>
                            </div>
                            <div className="col-sm-4 col-xs-12">
                                <div className="footerContent mid">
                                    <h5 className="title">Contact</h5>
                                    <br />
                                    <ul className="list-unstyled">
                                        <li>
                                            <i className="fa fa-home" aria-hidden="true" />
                                            P406 - D9 - Hanoi University Of Science And Technology
            </li>
                                        <li>
                                            <i className="fa fa-phone" aria-hidden="true" />
                                            <b>0225 3920920 - Fax: 02253 757125</b>
                                        </li>
                                        <li>
                                            <i className="far fa-envelope" aria-hidden="true" />
                                            <a href="mailTo:ictk60@gmail.com">ictk60@gmail.com</a>
                                        </li>
                                    </ul>
                                    <ul className="list-inline ">
                                        <li className="list-inline-item"><a href="http://facebook.com/xekhachhoanglong" title="Facebook Hoàng Long" target="_blank">
                                            <i className="fab fa-facebook fa-2x" aria-hidden="true" /></a>
                                        </li>
                                        <li className="list-inline-item"><a href="https://twitter.com/hoanglongbus" title="Twitter Hoàng Long" target="_blank"><i className="fab fa-twitter fa-2x" aria-hidden="true" /></a></li>
                                        <li className="list-inline-item"><a href="http://google.com/+xekhachhoanglong" title="Google Plus Hoàng Long" target="_blank"><i className="fab fa-google-plus fa-2x" aria-hidden="true" /></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-sm-3 col-xs-12">
                                <div className="footerContent right">
                                    <h5 className="title">IMAGES</h5>
                                    <br />
                                    <div className="row ">
                                        <div className="col-md-4 col-sm-6">
                                            <a className="fancybox-pop" href="/"><img src="images/a1.jpg" width="100%" alt="image" /></a>
                                        </div>
                                        <div className="col-md-4 col-sm-6">
                                            <a className="fancybox-pop" href="/"><img src="images/a2.jpg" width="100%" alt="image" /></a>
                                        </div>
                                        <div className="col-md-4 col-sm-6">
                                            <a className="fancybox-pop" href="/"><img src="images/a3.jpg" width="100%" alt="image" /></a>
                                        </div>
                                        <div className="col-md-4 col-sm-6">
                                            <a className="fancybox-pop" href="/"><img src="images/a4.jpg" width="100%" alt="image" /></a>
                                        </div>
                                        <div className="col-md-4 col-sm-6">
                                            <a className="fancybox-pop" href="/"><img src="images/a4.jpg" width="100%" alt="image" /></a>
                                        </div>
                                        <div className="col-md-4 col-sm-6">
                                            <a className="fancybox-pop" href="/"><img src="images/a4.jpg" width="100%" alt="image" /></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
        )
    }
}
