import React, { Component } from 'react';
import { Button, Steps, Icon, Popover } from 'antd';
import 'antd/dist/antd.css';
import '../../assets/css/bookticket.css';
import FindRoute from './FindRoute';
import FindRouteABC from './FindRoute';


const Step = Steps.Step;


class BookTicketStepper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: 0,
        };
    }

    next() {
        const current = this.state.current + 1;
        this.setState({ current });
    }

    prev() {
        const current = this.state.current - 1;
        this.setState({ current });
    }

    render() {

        const { current } = this.state;

        return (
            <div className="container bookticket-step">
                <Steps current={current}>
                    <Step description="Step 1" title="FindRoute" icon={<i className="fa fa-home" aria-hidden="true" />} > </Step>
                    <Step description="Step 2" title="Information" icon={<Icon type="solution" />} />
                    <Step description="Step 3" title="Confirm" icon={<Icon type="loading" />} ></Step>
                    <Step description="Step 4" title="Confirm" icon={<Icon type="smile-o" />} />
                </Steps>

                <div className="container box-shadow">
                    <FindRouteABC></FindRouteABC>
                </div>


                {
                    current < 5
                    && <Button type="primary" onClick={() => this.next()}>Next</Button>
                }

                {
                    current === 5
                    && <Button type="primary">Done</Button>
                }
                {
                    current > 0
                    && (
                        <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
                            Previous
                        </Button>
                    )
                }

            </div>

        )
    }
}

export default BookTicketStepper;