import React, { Component } from 'react';



class SliderShow extends Component {
    render() {

        return (
            <div class="slider text-center">
                <div class="callbacks_container">
                    <ul class="rslides" id="slider4">
                        <li>
                            <div class="slider-img one-img">
                                <div class="container">
                                    <div class="slider-info ">
                                        <h5>Choose the best route for you</h5>
                                        <div class="bottom-info">
                                            <p>We bring to you the best experience</p>
                                        </div>
                                        <div class="outs_more-buttn">
                                            <a href="about.html">Book Ticket</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="slider-img two-img">
                                <div class="container">
                                    <div class="slider-info ">
                                        <h5>You can go anywhere</h5>
                                        <div class="bottom-info">
                                            <p>We have many route for you to travell anywhere in Vietnam</p>
                                        </div>
                                        <div class="outs_more-buttn">
                                            <a href="about.html">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="slider-img three-img">
                                <div class="container">
                                    <div class="slider-info">
                                        <h5>Something</h5>
                                        <div class="bottom-info">
                                            <p>Something for you</p>
                                        </div>
                                        <div class="outs_more-buttn">
                                            <a href="about.html">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        );
    }
}

export default SliderShow;