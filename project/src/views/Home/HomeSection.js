import React, { Component } from 'react';
import '../../assets/css/home.css';

class HomeSection extends Component {
    render() {
        return (
            <div class="home-section">
                <section>
                    <div className="container goto_bookticket">
                        <div className="row">
                            <div className="col-md-6">
                                <a href="/" className="footer-logo"><img src=".././images/minivan.png" alt="logo-hl" width="60%" /></a>
                            </div>
                            <div id="home-book-ticket" className="col-md-6">
                                <h3 className="title">
                                    BOOK TICKET RIGHT NOW
          </h3>
                                <p>
                                    This is the best transport service that give you the best experience.
          </p>
                                <div className="button">
                                    <p className="btnText">BOOK TICKET</p>
                                    <div className="btnTwo">
                                        <p className="btnText2">GO!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div className="container card_goto">
                        <div className="row">
                            <div className="col-md-4 col-sm-6">
                                <div className="row home-card-page">
                                    <div className="col-md-5 image" style={{ background: 'rgb(11, 226, 198)' }}>
                                        <img src="https://kampus.vn/static/themes/default/images/kampus/intro/tc_ht_3.png" width="100%" />
                                    </div>
                                    <div className="col-md-7 content">
                                        <h6 style={{ color: 'rgb(34, 141, 141)' }}>-&gt; Go to ...</h6>
                                        <h5> SERVICE <br /> ROUTE </h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-6">
                                <div className="row home-card-page">
                                    <div className="col-md-5 image" style={{ background: 'rgb(255, 90, 159)' }}>
                                        <img src="https://kampus.vn/static/themes/default/images/kampus/intro/tc_ht_3.png" width="100%" />
                                    </div>
                                    <div className="col-md-7 content">
                                        <h6 style={{ color: 'rgb(214, 77, 180)' }}>-&gt; Go to ...</h6>
                                        <h5> ABOUT US </h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-6">
                                <div className="row home-card-page">
                                    <div className="col-md-5 image" style={{ background: '#FF8000' }}>
                                        <img src="https://kampus.vn/static/themes/default/images/kampus/intro/tc_ht_3.png" width="100%" />
                                    </div>
                                    <div className="col-md-7 content">
                                        <h6 style={{ color: 'rgb(202, 142, 86)' }}>-&gt; Go to ...</h6>
                                        <h5> SOMETHING </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default HomeSection;