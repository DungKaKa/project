import React, { Component } from 'react';
import SliderShow from './SliderShow';
import HomeSection from './HomeSection';
import '../../assets/css/home.css';
import '../../assets/css/style.css';
import '../../assets/js/slide.js';

class Home extends Component {
    render() {
        return (
            <div>
                <SliderShow></SliderShow>
                <HomeSection></HomeSection>
            </div>
        );
    }
}

export default Home;