import React, { Component } from 'react';
import AppHeader from './components/Header/AppHeader';
import Footer from './components/Footer/Footer';
import ModalLogin from './components/Login/ModalLogin';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import AppRouter from './routes/AppRouter';
import './assets/css/style.css';


class App extends Component {

    render() {
        return (
            <Router>
                <div>
                    <AppHeader></AppHeader>
                    <ModalLogin></ModalLogin>

                    <AppRouter></AppRouter>

                    <Footer></Footer>
                </div>
            </Router>
        );
    }
}

export default App;
